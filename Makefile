CC=gcc
NAME=jit
CFLAGS=-g
LIB=
DEFS=

all:
	$(CC) -o $(NAME) main.c $(CFLAGS) $(LIB)

.PHONY: all
