#include <stdlib.h>
#include <stdint.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdbool.h>

enum {
	RAX,
	RCX,
	RDX,
	RBX,
	RSP,
	RBP,
	RSI,
	RDI,
	REG_SIZE,
};

//boolean representation of if a register currently contains a value.
bool g_RegAlloc[REG_SIZE] = {0};

static inline void push_int32(char* buffer, int32_t a) {
	buffer[0] = 0x68;
	buffer[1] = a; //This doesn't look right, dont we have to write each byte manually?
}

char* push(char* buffer, int reg) {
	buffer[0] = 0x50 + reg;
	return &buffer[1];
}

char* push_imm8(char* buffer, uint8_t imm) {
	buffer[0] = 0x6a;
	buffer[1] = imm;
	return &buffer[2];
}

char* pop(char* buffer, int reg) {
	buffer[0] = 0x58 + reg;
	return &buffer[1];
}

char* mov(char* buffer, uint8_t offset) {
	//mov offset(%rsp), %eax
	buffer[0] = 0x8b;
	buffer[1] = 0x44;
	buffer[2] = 0x24;
	buffer[3] = offset;
	return &buffer[4];
}

char* mov_reg_mem(char* buffer, int reg, uint32_t offset) {
	int size = 0;
	
	buffer[size++] = 0x8b;
	buffer[size++] = (offset > 0) ? (0x44) : (0x04);
	buffer[size++] = 0x24;
	if(offset > 0) {
		buffer[size++] = (offset & 0xff);
		buffer[size++] = (offset & 0xff00) >> 8;
		buffer[size++] = (offset & 0xff0000) >> 16;
		buffer[size++] = (offset & 0xff000000) >> 24;
	}
	return &buffer[size];
}

char* routine_end(char* buffer) {
	int size = 0;

	//add 0x10, %rsp
	buffer[size++] = 0x48;
	buffer[size++] = 0x83;
	buffer[size++] = 0xc4;
	buffer[size++] = 0x10;

	//push %eax
	buffer[size++] = 0x50;
	return &buffer[size];
}

char* add_one(char* buffer, int32_t a) {
	int size = 0;

	push_int32(&buffer[size], a);
	size += sizeof(a) + 1;

	buffer = mov(&buffer[size], 8);
	size = 0;
	//add %eax,(%rsp)
	buffer[size++] = 0x03;
	buffer[size++] = 0x04;
	buffer[size++] = 0x24;
	
	buffer = routine_end(&buffer[size]);
	size = 0;
	return &buffer[size];
}

char* add(char* buffer, int32_t a, int32_t b) {
	int size = 0;

	push_int32(&buffer[size], a);
	size += sizeof(a) + 1;
	return add_one(&buffer[size], b);
}

char* sub_one(char* buffer, int32_t a) {
	int size = 0;

	push_int32(&buffer[size], a);
	size += sizeof(a) + 1;
	buffer = mov(&buffer[size], 8);
	size = 0;

	//sub (%rsp),eax
	buffer[size++] = 0x2b;
	buffer[size++] = 0x04;
	buffer[size++] = 0x24;
	
	buffer = routine_end(&buffer[size]);
	size = 0;
	return &buffer[size];
}

char* sub(char* buffer, int32_t a, int32_t b) {
	int size = 0;

	push_int32(&buffer[size], a);
	size += sizeof(a) + 1;
	return sub_one(&buffer[size], b);
}

char* mul_one(char* buffer, int32_t a) {
	int size = 0;

	push_int32(&buffer[size], a);
	size += sizeof(a) + 1;
	buffer = mov(&buffer[size], 8);
	size = 0;

	//sub $eax,(%rsp)
	buffer[size++] = 0x0f;
	buffer[size++] = 0xaf;
	buffer[size++] = 0x04;
	buffer[size++] = 0x24;
	
	buffer = routine_end(&buffer[size]);
	size = 0;
	return &buffer[size];
}

char* mul(char* buffer, int32_t a, int32_t b) {
	int size = 0;

	push_int32(&buffer[size], a);
	size += sizeof(a) + 1;
	return mul_one(&buffer[size], b);
}

char* div_one(char* buffer, int32_t a) {
	int size = 0;

	push_int32(&buffer[size], a);
	size += sizeof(a) + 1;
	buffer = mov(&buffer[size], 8);
	size = 0;

	//mov $0,%rdx
	buffer[size++] = 0x48;
	buffer[size++] = 0xc7;
	buffer[size++] = 0xc2;
	buffer[size++] = 0x00;
	buffer[size++] = 0x00;
	buffer[size++] = 0x00;
	buffer[size++] = 0x00;

	//idiv $eax,(%rsp)
	buffer[size++] = 0xf7;
	buffer[size++] = 0x3c;
	buffer[size++] = 0x24;
	
	buffer = routine_end(&buffer[size]);
	size = 0;
	return &buffer[size];
}

char* div_op(char* buffer, int32_t a, int32_t b) {
	int size = 0;

	push_int32(&buffer[size], a);
	size += sizeof(a) + 1;
	return div_one(&buffer[size], b);
}

char* cmp_op(char* buffer, int8_t offset) {
	int size = 0;

	buffer[size++] = 0x3b;
	buffer[size++] = (offset > 0) ? (0x44) : (0x04);
	buffer[size++] = 0x24;
	if(offset > 0) {
		buffer[size++] = offset;
	}
	return &buffer[size];
}

char* cmp_imm8(char* buffer, int8_t imm) {
	buffer[0] = 0x83;
	buffer[1] = 0x3c;
	buffer[2] = 0x24;
	buffer[3] = imm;
	return &buffer[4];
}

char* jne(char* buffer, uint32_t offset) {
	buffer[0] = 0x0f;
	buffer[1] = 0x85;
	buffer[2] = offset & 0xff;
	buffer[3] = (offset & 0xff00) >> 8;
	buffer[4] = (offset & 0xff0000) >> 16;
	buffer[5] = (offset & 0xff000000) >> 24;
	return &buffer[6];
}

char* call_op(char* buffer, void* ptr) {
	int size = 0;
	int64_t offset = (int64_t)ptr;

	//registers get smashed we need to save them onto the stack.

	buffer = push(buffer, RAX);
	//movabs rbx, offset
	buffer[size++] = 0x48;
  	buffer[size++] = 0xbb;
	buffer[size++] = offset & 0xff;
	buffer[size++] = (offset & 0xff00) >> 8;
	buffer[size++] = (offset & 0xff0000) >> 16;
	buffer[size++] = (offset & 0xff000000) >> 24;
	buffer[size++] = (offset & 0xff00000000) >> 32;
	buffer[size++] = (offset & 0xff0000000000) >> 40;
	buffer[size++] = (offset & 0xff000000000000) >> 48;
	buffer[size++] = (offset & 0xff00000000000000) >> 56;

	//call rbx
	buffer[size++] = 0xff;
	buffer[size++] = 0xd3;
	buffer = pop(&buffer[size], RAX);
	return buffer;
}

char* ret(char* buffer, int vars) {
	int size = 0;

	//sub %rsp, 0x8
	buffer[size++] = 0x48;
	buffer[size++] = 0x83;
	buffer[size++] = 0xc4;
	buffer[size++] = vars * 0x8;
	//ret
	buffer[size++] = 0xC3;
	return &buffer[size];
}

void test() {
	printf("Hello World!\n");
}

int main() {
	uint8_t *mem = (uint8_t*) mmap(NULL, 1024 * 4, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	uint8_t* buffer = mem;
	uint8_t* lbl_loop = NULL;
	int (*func)(int) = (int(*)(int))mem;
	//FIXME: argument passed to func does not change loop count.

	buffer = push(buffer, RDI);
	buffer = push_imm8(buffer, 0);//Loop counter.
	lbl_loop = buffer;//Store address.
	buffer = mov_reg_mem(buffer, RAX, 0); //mov [rsp] to rax.
	buffer = add_one(buffer, 1);//sub 1 from rax
	buffer = call_op(buffer, test);
	buffer = cmp_imm8(buffer, 10); //compare rax and rsp
	buffer = jne(buffer, (uint32_t)(lbl_loop - (buffer + 6)));
	buffer = ret(buffer, 2);
	printf("Value is %d \n", func(10));
	return EXIT_SUCCESS;
}
